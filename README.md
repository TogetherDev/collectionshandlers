# Collections Handlers #

#Versions

* **1.0**

## What is it?

This is a utility library, builded with [JQuery](https://jquery.com/), for facilitate the handle data collections. It's provides the following functions:

Function  | Description
--------------------------------------- | -------------
$("selector").getValue                  | Gets the value or the text of an element.
$.getValuesAsArray                      | Returns the values of the elements, that matches with the given selectors array.
$.getMappedValues                       | Returns a map with the values of the elements, that matches with the given selectors array.
$.mapValuesToOtherElements              | Maps the values of the elements, that matches by the each key of the given map, to respective element, that matches with the key value.
$("selector").setValue                  | Sets the value or the text of an element.
$.setValues                             | Sets the value or the text of the elements, that matches by the each key of the given map, to respective key value.
$("selector").addTableRow               | Adds a new `tr` element to selected table.
$("selector").addListItem               | Adds a new `li` element to selected list.
$("selector").removeClosestParent       | Removes the closest parent.
$("selector").removeCurrentTableRow     | Removes the closest `tr` parent.
$("selector").removeCurrentListItem     | Removes the closest `li` parent.

## Using Collections Handlers

You can use an direct link, that is easier, or you can download it and later add to your project.

## Licensing

**Collections Handlers** is provided and distributed under the [Apache Software License 2.0](http://www.apache.org/licenses/LICENSE-2.0).

Refer to *LICENSE.txt* for more information.
