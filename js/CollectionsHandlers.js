/*
Copyright 2017 Thomás Sousa Silva.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

$.fn.getValue = function (clear = false) {
    var element = $(this);
    if (element.is("input") || element.is("select") || element.is("textarea")) {
        var value = element.val();
        element.val("");
        return value;
    } else {
        var text = element.text();
        if (clear) {
            element.text("");
        }
        return text;
}
};

$.getValuesAsArray = function (selectorsArray, clearSource = false, useSameArray = false) {
    var array = (useSameArray ? selectorsArray : []);
    for (var i = 0; i < selectorsArray.length; i++) {
        array[i] = $(selectorsArray[i]).getValue(clearSource);
    }
    return array;
};

$.getMappedValues = function (selectorsMap, clearSource = false, useSameObject = false) {
    var map = (useSameObject ? selectorsMap : {});
    for (var key in selectorsMap) {
        map[key] = $(selectorsMap[key]).getValue(clearSource);
    }
    return map;
};

$.mapValuesToOtherElements = function (selectorsMap, clearSource = false) {
    for (var key in selectorsMap) {
        var value = $(key).getValue(clearSource);
        $(selectorsMap[key]).setValue(value);
}
};

$.fn.setValue = function (value) {
    var element = $(this);
    if (element.is("input") || element.is("select")) {
        element.val(value);
    } else if (!element.is("select")) {
        element.text(value);
    }
    return this;
};

$.setValues = function (selectorsMap) {
    for (var key in selectorsMap) {
        $(key).setValue(selectorsMap[key]);
    }
};

$.fn.addTableRow = function (dataArray, isSelectors = false, clearSource = true, fadeInOptions) {
    var tr = $("<tr/>");
    for (var i = 0; i < dataArray.length; i++) {
        var value = dataArray[i];
        if (isSelectors && (typeof value === "string")) {
            value = $(value).getValue(clearSource);
        }
        tr.append($("<td/>").append(value));
    }
    if (fadeInOptions !== undefined) {
        tr.hide();
        $(this).append(tr);
        tr.fadeIn(fadeInOptions);
    } else {
        $(this).append(tr);
    }
    return this;
};

$.fn.addListItem = function (dataArray, isSelectors = false, clearSource = true, fadeInOptions) {
    var listItem = $("<li/>");
    for (var i = 0; i < dataArray.length; i++) {
        var value = dataArray[i];
        if (isSelectors && (typeof value === "string")) {
            value = $(value).getValue(clearSource);
        }
        listItem.append(value);
    }
    if (fadeInOptions !== undefined) {
        listItem.hide();
        $(this).append(listItem);
        listItem.fadeIn(fadeInOptions);
    } else {
        $(this).append(listItem);
    }
    return this;
};

$.fn.removeClosestParent = function (selector, fadeOutOptions) {
    var parrent = $(this).closest(selector);
    if (fadeOutOptions !== undefined) {
        fadeOutOptions["complete"] = function () {
            parrent.remove();
        };
        parrent.fadeOut(fadeOutOptions);
    } else {
        parrent.remove();
    }
    return this;
};

$.fn.removeCurrentTableRow = function (fadeOutOptions) {
    $(this).removeClosestParent("tr", fadeOutOptions);
    return this;
};

$.fn.removeCurrentListItem = function (fadeOutOptions) {
    $(this).removeClosestParent("li", fadeOutOptions);
    return this;
};
